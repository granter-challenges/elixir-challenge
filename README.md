![Granter](https://granter.com.br/wp-content/uploads/2018/08/logo-granter.svg)


# Desafio Backend Elixir

Este desafio visa avaliar sua capacidade de resolver problemas, buscar soluções e demonstrar seu conhecimento em Elixir, bem como suas habilidades com as ferramentas Ecto e Phoenix, além de seu domínio do banco de dados PostgreSQL

Para a entrega do desafio deve ser criado um repositório `git` **privado** no 
[GitLab](https://www.gitlab.com) [adicionando como membros](https://docs.gitlab.com/ee/user/project/members/#add-users-to-a-project) com a permissão de `Developer` para os emails:
- felippe.jose@granter.com.br
- luiz.klein@granter.com.br
- rodrigo.ce.moretto@gmail.com

**Importante**: mesmo que não tenha finalizado o desafio encorajamos que o envie até onde foi possível completar.

## Objetivo

O principal objetivo deste projeto é desenvolver um sistema onde o relacionamento entre as encomendas (Parcels) e localizações (Locations) são realizadas [por esta estrutura de grafos](#sobre-o-sistema-de-grafos), que registram o percurso das encomendas.

Isso deve ser feito por meio de um backend que disponibilize endpoints HTTPs que servem JSON, utilizando Phoenix, Ecto e PostgreSQL, para simular este sistema de encomendas.

As encomendas podem ser transferidas múltiplas vezes para qualquer local. No entanto, ao chegarem ao destino final, não será possível realizar mais nenhuma transferência.

Por exemplo:
1. Uma encomenda foi criada com origem 'Location A' e com o destino 'Location D'
2. A encomenda é transferida chegando no 'Location B'
3. A encomenda é transferida chegando no 'Location C'
4. A encomenda é transferida chegando no 'Location D'
    4.1. A encomenda chegou no seu destino e não pode mais ser transferida

No final desse exemplo os locais na qual a encomenda passou são:
```
1. Location A
2. Location B
3. Location C
4. Location D
```

Nota que esse é apenas um exemplo, as transferências podem ser feitas para qualquer
local e não necessariamente tem uma ordem pré determinada, a seguinte lista também é
um caminho de encomenda válido:
```
1. Location A
2. Location H
3. Location A
4. Location B
5. Location V
6. Location D
```

As transferências serão todas feitas via API no endpoint ['Transfer parcel'](#transfer-parcel).

## Sobre o sistema de grafos
Para representar os locais que uma encomenda passou **deverá** ser utilizado um sistema de grafos. Na implementação os vértices **devem representar apenas** Locations do banco de dados, enquanto as movimentações **devem ser representadas** via arestas. Por exemplo:

![image info](./elixir_challenge_exemplo_movimentacao.webp)

No exemplo acima os locais onde a encomenda passou foi:
```
1. Location A
2. Location B
3. Location C
4. Location B
5. Location C
6. Location D
```

Note que é possível que grafos ciclicos sejam criados.

As arestas e vértices dos Grafos devem ser salvas em tabelas separadas no banco de dados. 

Além dos relacionamentos criados via o sistema de grafos descritos acima não pode haver nenhum relacionamento entre Locations e Parcels, isso inclui  `many 2 many`s.

Para carregar esses grafos duas possibilidades podem ser utilizadas

### CTE Recursiva (preferível)
A primeira opção para carregar os grafos para ser utilizado no código é via [CTE Recursiva](https://www.postgresql.org/docs/16/queries-with.html#QUERIES-WITH-RECURSIVE) do banco de dados onde os caminhos do Parcel são carregados diretamente via query.

### Código
Você pode carregar as vertices e arestas e fazer as operações de grafos via Elixir, porém não pode ser utilizado livrarias externas para esse intuito.

## Banco de Dados
Nesse backend duas tabelas principais **devem** estar presentes:

> Aviso!
> As tabelas e colunas descritas a seguir (Locations e Parcel) **são obrigatórias**, porém, você está **livre para adicionar** novas **colunas** ou
> **tabelas** que achar necessário para melhor resolver o problema em questão seguindo as regras descritas anteriormente.

### Locations:
Tabela que representa locais por onde encomendas podem passar.
Essa entidade deve possuir as seguintes colunas:
    - `id` - Chave primaria
    - `name` - Nome do local.

Os dados dessa tabela devem ser criados randomicamente e inserido via `seeds` com o comando:

```bash
mix run priv/repo/seeds.exs
```

O script `seeds` deve adicionar pelo menos **100** linhas na tabela `locations`.

### Parcel
Tabela que representa uma encomenda, com as seguintes colunas:
    - `id` - Chave primaria
    - `description` - Descrição da encomenda
    - `source_id` - FK relacionando a tabela `location` que define qual é a origem da encomenda
    - `destination_id` - FK relacionando a tabela `location` que define qual é o destino final da encomenda
    - `is_delivered` - Booleana que define se a encomenda chegou no seu destino ou não

## Endpoints
A API JSON deverá conter os seguintes endpoints:

> Aviso!
> Os JSONs de exemplo são apenas uma sugestão, você está livre para retornar os dados da forma que achar melhor
> desde que siga as regras definidas pelos endpoints.

### Create Parcel
Endpoint para criação de encomendas:

Caminho: `/api/parcel`
Método: `POST`
Exemplo de JSON de criação
```json
{
    "description": "Minha encomenda maneira 🛹",
    "source_id": "<location-id>",
    "destination_id": "<location-id>"
}
```

### Get Parcel
Endpoint para pegar informações de um `parcel`, retornando também as `locations` pelas quais o `parcel` passou.
As `locations` devem ser retornadas em **ordem de movimentação**, por exemplo, se um `parcel` passou pela
`Location A` e depois pela `Location B`, o retorno deverá ser: 
```json
[
    {"id": "<location-id>", "name": "Location A"},
    {"id": "<location-id>", "name": "Location B"}
]
```

Caminho: `/api/parcel/<parcel-id>`
Método: `GET`
Exemplo de retorno JSON
```json
{
    "id": "<parcel-id>",
    "description": "Minha encomenda maneira 🛹",
    "is_delivered": false,
    "source": {
        "id": "<location-id>",
        "name": "Location A"
    },
    "destination": {
        "id": "<location-id>",
        "name": "Location C"
    },
    "locations": [
        {"id": "<location-id>", "name": "Location A"},
        {"id": "<location-id>", "name": "Location B"}
    ]
}
```

### Transfer parcel
Endpoint para transferir um `parcel` para uma `location`. Esse endpoint deve apenas receber o `id` 
do `parcel` (definido no caminho do endpoint) e o `id` da `location` de transferência.

Caminho: `/api/parcel/<parcel-id>/transfer`
Método: `POST`
Exemplo de JSON de criação
```json
{
    "transfer_location_id": "<location-id>"
}
```

### Get Location
Endpoint para pegar informações de uma `location`, retornando também os `parcels` que estão **atualmente** nessa `location`.

Caminho: `/api/location/<location-id>`
Método: `GET`
```json
{
    "id": "<location-id>",
    "name": "Location B",
    "parcels": [
        {
            "id": "<parcel-id>",
            "description": "Minha encomenda maneira 🛹",
            "is_delivered": false,
            "source": {
                "id": "<location-id>",
                "name": "Location A"
            },
            "destination": {
                "id": "<location-id>",
                "name": "Location C"
            }
        }
    ]
}
```

## Validando o backend
Para a entrega do projeto é necessário disponibilizar uma das seguintes opções, 
para fazer testes:

### Testes com ExUnit (preferível)
Disponibilizar testes de integração fazendo a criação e transferência de `parcels`, checando se as 
transferências estão corretas, por exemplo, para cada transferência ser verificado se o `parcel` se 
encontra na `location` correta.

ou

### Arquivos JSONs
Disponibilizar arquivos `.json` com corpo de requests para API simulando a criação e transferência 
de `parcels`.


> Aviso!
> Para garantir o funcionamento do backend, teste com no **no mínimo** duas encomendas com 
> 5 ou mais transferências.
